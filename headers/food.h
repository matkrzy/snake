//
// Created by Mateusz on 2015-11-24.
//

#ifndef SNAKEFINAL_FOOD_H
#define SNAKEFINAL_FOOD_H

#include "console.h"

class food {
public:
    unsigned __int16 foodX = NULL, foodY = NULL;

    void generateFood(unsigned __int16 consoleRows, unsigned __int16 consoleColumns);

    void showFood(console consoleManager);
};


#endif //SNAKEFINAL_FOOD_H
