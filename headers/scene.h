//
// Created by Mateusz on 2015-11-24.
//

#ifndef SNAKEFINAL_SCENE_H
#define SNAKEFINAL_SCENE_H

#include <console.h>
#include <food.h>
#include <snake.h>
#include "records.h"
#include <inttypes.h>



class scene {
public:
    console consoleManager;
    food foodManager;
    snake snakeManager;
    records recordsManager;
    bool gameStatus = true;

    unsigned __int16  dirKey = NULL;

    scene();

    void setSnakePosition();
    void newGame();
    void selectOnEndGame();

};

#endif //SNAKEFINAL_SCENE_H
