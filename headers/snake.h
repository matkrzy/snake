//
// Created by Mateusz on 2015-11-24.
//

#ifndef SNAKEFINAL_SNAKE_H
#define SNAKEFINAL_SNAKE_H


#define KEY_UP 72
#define KEY_DOWN 80
#define KEY_LEFT 75
#define KEY_RIGHT 77

#include <vector>
#include <windows.h>
#include <conio.h>
#include <string>
#include "console.h"
#include "food.h"

using namespace std;

class pieceSnake {
public:
    unsigned __int16 x;
    unsigned __int16 y;

    pieceSnake(unsigned __int16 xx, unsigned __int16 xy) {
        this->x = xx;
        this->y = xy;
    }
};


class snake {

public:
    unsigned __int16 positionX = NULL;
    unsigned __int16 positionY = NULL;
    unsigned __int16 snakeSize = 1;
    string playerName;
    unsigned __int16 points = 0;
    vector<pieceSnake> bodySnake;

    void snakeSettings();

    void snakeInit(console consoleManager);

    void snakeMake(console consoleManager);

    void snakeMove(console &consoleManager, food &foodManager, unsigned __int16 &dirKey);

    void snakeCollision(food &foodManager, console &consoleManager,unsigned __int16 &dirKey);

};

#endif //SNAKEFINAL_SNAKE_H
