//
// Created by Mateusz on 2015-11-24.
//

#ifndef SNAKEFINAL_CONSOLE_H
#define SNAKEFINAL_CONSOLE_H

#include <inttypes.h>

class console {

public:
    unsigned __int16 consoleRows=0, consoleColumns=0;
    unsigned __int16 breakTime = 200;

    void getConsoleSize();

    void removeScrollbar();

    void gotoxy( unsigned __int16 x,  unsigned __int16 y);
};

#endif //SNAKEFINAL_CONSOLE_H
