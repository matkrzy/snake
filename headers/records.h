//
// Created by Mateusz on 2015-11-25.
//

#ifndef SNAKEFINAL_FILEAPI_H
#define SNAKEFINAL_FILEAPI_H

#include <string>
#include <snake.h>

using namespace std;

class records {
private:
    string fileName ="records.txt";

public:
    void readRecords();
    void saveRecord(snake snakeManager);

};

#endif //SNAKEFINAL_FILEAPI_H
