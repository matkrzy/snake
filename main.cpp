#include <iostream>
#include <scene.h>
#include <time.h>

using namespace std;

int main() {
    srand(time(NULL));


    scene game;


    __int16 tempKeys[2];//index 0 is previous key index 1 is current key

    while (game.gameStatus) {
        while (game.dirKey != 27 && game.dirKey != 9) {
            Sleep(game.consoleManager.breakTime);//delay game speed


            if (game.dirKey != 224)
                tempKeys[0] = game.dirKey;

            if (kbhit()) {
                game.dirKey = getch();//get pressed key
                if (game.dirKey != 224) {
                    tempKeys[1] = game.dirKey;
                }
            }


            switch (game.dirKey) {
                case KEY_UP:
                    if (tempKeys[0] == KEY_DOWN && game.snakeManager.snakeSize > 1) {
                        game.dirKey = tempKeys[0];//if last dir was opposite dir -> set correct dir and break
                        break;
                    }
                    game.snakeManager.snakeMove(game.consoleManager, game.foodManager, game.dirKey);
                    break;
                case KEY_DOWN:
                    if (tempKeys[0] == KEY_UP && game.snakeManager.snakeSize > 1) {
                        game.dirKey = tempKeys[0];//if last dir was opposite dir -> set correct dir and break
                        break;
                    }
                    game.snakeManager.snakeMove(game.consoleManager, game.foodManager, game.dirKey);
                    break;
                case KEY_RIGHT:
                    if (tempKeys[0] == KEY_LEFT && game.snakeManager.snakeSize > 1) {
                        game.dirKey = tempKeys[0];//if last dir was opposite dir -> set correct dir and break
                        break;
                    }
                    game.snakeManager.snakeMove(game.consoleManager, game.foodManager, game.dirKey);
                    break;
                case KEY_LEFT:
                    if (tempKeys[0] == KEY_RIGHT && game.snakeManager.snakeSize > 1) {
                        game.dirKey = tempKeys[0];//if last dir was opposite dir -> set correct dir and break
                        break;
                    }
                    game.snakeManager.snakeMove(game.consoleManager, game.foodManager, game.dirKey);
                    break;
                default:
                    break;
            }
        }

        //PAUSE WHEN USER PRESS ESC KEY
        if (game.dirKey == 27) {
            system("cls");
            system("pause");//system wait for key
            game.dirKey = tempKeys[0];//sets previous direction
        }

        //END GAME DISPLAY MENU
        if (game.dirKey == 9) {
            system("cls");
            game.consoleManager.gotoxy(0, 0);
            cin.clear();
            cout << "Koniec gry. Zdrobyles " << game.snakeManager.points << " punktow!";
            game.recordsManager.saveRecord(game.snakeManager);

            game.selectOnEndGame();
        }
    }
    system("pause");
    return 0;
}