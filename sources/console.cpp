//
// Created by Mateusz on 2015-11-24.
//

#include "console.h"
#include <iostream>
#include <windows.h>

using namespace std;


void console::getConsoleSize() {
    CONSOLE_SCREEN_BUFFER_INFO csbi;

    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
    this->consoleColumns = csbi.srWindow.Right - csbi.srWindow.Left + 1;//set consoleColumns field
    this->consoleRows = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;//set consoleRows field
}

void console::removeScrollbar() {
    HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_SCREEN_BUFFER_INFO info;
    GetConsoleScreenBufferInfo(handle, &info);
    COORD new_size =
            {
                    info.srWindow.Right - info.srWindow.Left + 1,
                    info.srWindow.Bottom - info.srWindow.Top + 1
            };
    SetConsoleScreenBufferSize(handle, new_size);
}

void console::gotoxy(unsigned __int16 x,  unsigned __int16 y) {
    COORD coord;
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(
            GetStdHandle(STD_OUTPUT_HANDLE),
            coord
    );
}


