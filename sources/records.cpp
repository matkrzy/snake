//
// Created by Mateusz on 2015-11-25.
//
#include <records.h>
#include <fstream>
#include <iostream>
#include <time.h>
#include <c++/iomanip>


void records::readRecords() {

    ifstream file;
    file.open(this->fileName, fstream::out);
    string line;

    if (file.is_open()) {
        while (getline(file, line)) {
            cout << line << endl << endl;
        }
        file.close();
    }
}

void records::saveRecord(snake snakeManager) {
    ofstream file;

    string name = snakeManager.playerName;
    unsigned __int16 points = snakeManager.points;

    //GET DATE
    time_t t = time(0);   // get time now
    struct tm *now = localtime(&t);

    //TRANSFORM POINTS TO STRING FOR LENGHT
    char buffer[64];
    unsigned __int16 pointsLenght = sprintf(buffer, "%d", points);

    file.open(this->fileName, ios::out | ios::app);
    if (file.is_open()) {
        file << name << setw(30 - name.length()) << points << setw(10 - pointsLenght) << now->tm_mday <<
        "-" <<
        (now->tm_mon + 1) << "-" <<
        (now->tm_year + 1900) << endl;


    }

    file.close();
}

