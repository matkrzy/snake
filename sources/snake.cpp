//
// Created by Mateusz on 2015-11-24.
//

#include <snake.h>
#include <iostream>

using namespace std;


void snake::snakeSettings() {
    string tempName;
    do {
        cout << "Podaj imie gracza max(20 znakow): ";
        cin >> tempName;
    } while (tempName.length() > 20);

    this->playerName = tempName;
    system("cls");
}

void snake::snakeInit(console consoleManager) {
    unsigned __int16 positionX = consoleManager.consoleColumns / 2;
    unsigned __int16 positionY = consoleManager.consoleRows / 2;

    this->positionX = positionX;
    this->positionY = positionY;

    this->bodySnake.insert(this->bodySnake.begin(), pieceSnake(positionX, positionY));
}

void snake::snakeMake(console consoleManager) {
    HANDLE hOut;
    hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    for (int i = 0; i < this->bodySnake.size(); i++) {
        if (i == 0)
            SetConsoleTextAttribute(hOut, 0x0004);

        unsigned __int16 x = this->bodySnake.at(i).x;
        unsigned __int16 y = this->bodySnake.at(i).y;
        consoleManager.gotoxy(x, y);
        cout << "O";
        SetConsoleTextAttribute(hOut, 0x0007);
    }
}

void snake::snakeMove(console &consoleManager, food &foodManager, unsigned __int16 &dirKey) {

    switch (dirKey) {
        case KEY_UP:
            this->positionY -= 1;
            break;
        case KEY_DOWN:
            this->positionY += 1;
            break;
        case KEY_RIGHT:
            this->positionX += 1;
            break;
        case KEY_LEFT:
            this->positionX -= 1;
            break;
    }

    this->bodySnake.insert(this->bodySnake.begin(), pieceSnake(positionX, positionY));//add an item -> at the beginning of the list
    this->bodySnake.pop_back();//remove last element of the list
    system("cls");

    snakeCollision(foodManager, consoleManager, dirKey);//detect collision with edges and food (if the food is eaten, generate new)

    consoleManager.gotoxy(0,0);
    cout<<"PUNKTY: "<<this->points<<"\t"<<"Opoznienie :"<<consoleManager.breakTime<<"ms";


    foodManager.showFood(consoleManager);//show food
    this->snakeMake(consoleManager);//draw snake by vector list
}

void snake::snakeCollision(food &foodManager, console &consoleManager, unsigned __int16 &dirKey) {

    unsigned __int16 foodX = foodManager.foodX;
    unsigned __int16 foodY = foodManager.foodY;

    //check collision with a food item
    if (foodX == this->positionX && foodY == this->positionY) {
        this->bodySnake.insert(this->bodySnake.begin(), pieceSnake(positionX, positionY));//add a new item
        this->snakeSize++;

        this->points += 10;//add 10 points after collision
        consoleManager.breakTime -= 5;//cut down breakTime, speed up snake play
        foodManager.generateFood(consoleManager.consoleRows, consoleManager.consoleColumns);//generate a new item

    }

    unsigned __int16 snakeX = this->positionX;
    unsigned __int16 snakeY = this->positionY;

    unsigned __int16 maxX = consoleManager.consoleColumns;
    unsigned __int16 maxY = consoleManager.consoleRows;

    //collision with the edges of a window
    if ((snakeX == 0 || snakeY == 0) || (snakeX == maxX || snakeY == maxY)) {
        dirKey = 9;
    }

    //check collision with the body
    if (this->bodySnake.size() > 3) {
        for (int i = 2; i < this->bodySnake.size(); i++) {

            if (this->bodySnake[i].x == snakeX && this->bodySnake[i].y == snakeY)
                dirKey = 9;
        }
    }
}