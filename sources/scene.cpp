//
// Created by Mateusz on 2015-11-24.
//

#include <iostream>
#include <scene.h>


using namespace std;


scene::scene() {
    consoleManager.removeScrollbar();
    consoleManager.getConsoleSize();


    this->snakeManager.snakeSettings();//set player name
    this->snakeManager.snakeInit(this->consoleManager);//set first snake position
    setSnakePosition();//draw snake

    this->foodManager.generateFood(this->consoleManager.consoleRows, this->consoleManager.consoleColumns);
    this->foodManager.showFood(this->consoleManager);
}

void scene::setSnakePosition() {
    this->snakeManager.snakeMake(this->consoleManager);
}

void scene::newGame() {
    this->snakeManager.bodySnake.clear();
    this->snakeManager.points = 0;
    this->snakeManager.snakeSize = 0;

    this->consoleManager.breakTime = 200;
    this->dirKey = 0;
    system("cls");

    this->snakeManager.snakeInit(this->consoleManager);
    setSnakePosition();
    this->foodManager.generateFood(this->consoleManager.consoleRows, this->consoleManager.consoleColumns);
    this->foodManager.showFood(this->consoleManager);
}

void scene::selectOnEndGame() {
    //system("cls");
    cout<<endl;
    cout<<endl;
    cout << "Menu:" << endl;
    cout << "[1] Zagraj jeszcze raz" << endl;
    cout << "[2] Wyjscie z gry" << endl;
    cout << "[3] Przegladaj rekordy" << endl;
    cout << "Twoj wybor: ";

    unsigned __int16 option;
    do {
        cin >> option;
        if (option < 1 || option > 3) {
            cout << "brak opcji w menu, prosze wybrac jeszcze raz";
        }
    } while (option < 1 || option > 3);

    switch (option) {
        case 1:
            this->newGame();
            break;
        case 2:
            this->gameStatus = false;
            break;
        case 3:
            system("cls");
            this->recordsManager.readRecords();
            cout << "Nacisnij dowolny klawisz aby powrocic do menu";

            getch();

            this->selectOnEndGame();
            break;
    }
}