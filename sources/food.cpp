//
// Created by Mateusz on 2015-11-24.
//

#include <stdlib.h>
#include "food.h"
#include <windows.h>
#include <iostream>
#include <time.h>

using namespace std;


void food::generateFood( unsigned __int16 consoleRows,  unsigned __int16 consoleColumns) {

    unsigned __int16 randX = (rand() % (consoleColumns - 2)) + 2;
    unsigned __int16 randY = (rand() % (consoleRows - 2)) + 2;

    this->foodX = randX;
    this->foodY = randY;
}

void food::showFood(console consoleManager) {

    HANDLE hOut;
    hOut = GetStdHandle(STD_OUTPUT_HANDLE);

    consoleManager.gotoxy(this->foodX, this->foodY);

    SetConsoleTextAttribute(hOut, 0x0004);
    cout << "#";
    SetConsoleTextAttribute(hOut, 0x0007);
}